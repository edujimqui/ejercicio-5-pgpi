# Ejercicio 5 PGPI

Este es el proyecto para la asignatura PGPI, ejercicio 5 del bloque de control de versiones. 

# Descripción del proyecto

El proyecto se basará en una aplicación de escritorio. Se tratará de  un programa que muestre información sobre los componentes del PC en el que se use dicha aplicación (CPU, memoria, tarjeta de vídeo...). Además se intentará aumentar la funcionalidad del mismo añadiendo otro tipo de opciones.

# Especificaciones y objetivos del proyecto

Para realizar este proyecto usaremos el IDE Visual Studio 2019 y el lenguaje C# (Windows Forms), cuyo paquete podemos instalar en el IDE. Los objetivos del proyecto serán:

1. Objetivos docentes:

 - Realizar una aplicación de escritorio funcional.
 - Usar la herramienta git para el control del proyecto.

2. Objetivos técnicos:

 - Realizar la estética de la aplicación (botones, opciones desplegables, cuadros de texto...).
 - Mostrar información básica de los componentes hardware del PC.
 - Mostrar información básica de los componentes software del PC.
 - Añadir funcionalidades adicionales.

# Cómo colaborar

Usaremos el mismo método de trabajo que hemos visto en la asignature PGPI. Primero, debemos hacer un fork con el botón corresponidente desde este proyecto para copiar este proyecto directamente en nuestra cuenta de usuario de GitLab. Una vez hecho esto, usamos `$ git clone "enlace"` para clonar esta copia a nuestro equipo, haciendo así un repositorio local. En este mismo repositorio, deberemos añadir otro remoto que corresponderá directamente con el proyecto original en GitLab usando `$ git remote add upstream "enlace de clone del repositorio ORIGINAL de GitLab"`. Con esto, podremos hacer push del repositorio original siempre antes de trabajar en nuevas colaboraciones. La forma de colaborar será la vista en PGPI: para cada nuevo cambio, crear una nueva rama y trabajar sobre ella. Hacer un pull de esta rama a nuestro repositorio de GitLab, y posteriormente desde este hacer el merge request correspondiente.

# Miembros del proyecto

* Alberto Moreno Monrobe
* Jesús Lobato Salas
* Simón Juan Rodríguez
* Eduado Jimenez Quintero

# Tareas asignadas

En terminos del uso de Git, Alberto Moreno Monrobe será el propietario del proyecto original en GitLab, y será el encargado de realizar los merge request de los demás colaboradores, además de contribuir como los demás miembros.

Todos los miembros del proyecto colaborarán de igual manera en todos los aspectos del mismo. Se deberán usar las herramientas correspondientes en git para informar de errores y colaborar. 

# Licencias de uso

Todas las herramientas usadas en este proyecto son **OPEN SOURCE**.
